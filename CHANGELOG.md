# 1.2.0 - unreleased

- [feature] Add a repeat button on the Player view thanks @discoverti
- [feature] Add translations mecanism + french translation
- [bug] Avoid duplicate song in "Recently Play" thanks @discoverti

# 1.0.1 - released 17/02/2022

- [bug] Launch album in home crash app

# 1.0.0 - released 17/02/2022

- First release in open-store.
