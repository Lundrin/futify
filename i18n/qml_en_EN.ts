<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>About</name>
    <message>
        <location filename="../qml/pages/About.qml" line="7"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="27"/>
        <source>App:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="34"/>
        <source>Futify is an unofficial native spotify client.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="46"/>
        <source>issues</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="59"/>
        <source>Author:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Albums</name>
    <message>
        <location filename="../qml/components/Albums.qml" line="66"/>
        <source>Play</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Artists</name>
    <message>
        <location filename="../qml/components/Artists.qml" line="58"/>
        <source>popularity</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Home</name>
    <message>
        <location filename="../qml/pages/Home.qml" line="74"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="89"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="94"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="122"/>
        <source>Your playlists</source>
        <translation>Your playlists</translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="144"/>
        <source>Your albums</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="166"/>
        <source>Recently Play</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="182"/>
        <source>Featured playlist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="204"/>
        <source>Top tracks</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Login</name>
    <message>
        <location filename="../qml/pages/Login.qml" line="34"/>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Login.qml" line="40"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Login.qml" line="53"/>
        <source>Log in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Login.qml" line="67"/>
        <source>spotify.com</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PlayerView</name>
    <message>
        <location filename="../qml/components/PlayerView.qml" line="72"/>
        <source>No song</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PlaylistView</name>
    <message>
        <location filename="../qml/components/PlaylistView.qml" line="82"/>
        <source>followers</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Playlists</name>
    <message>
        <location filename="../qml/components/Playlists.qml" line="66"/>
        <source>Play</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Queue</name>
    <message>
        <location filename="../qml/pages/Queue.qml" line="30"/>
        <source>Queue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="34"/>
        <source>Next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="42"/>
        <source>Play/Pause</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="54"/>
        <source>Precedent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="62"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="71"/>
        <source>Repeat</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Search</name>
    <message>
        <location filename="../qml/pages/Search.qml" line="35"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Search.qml" line="51"/>
        <source>Tracks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Search.qml" line="60"/>
        <source>Albums</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Search.qml" line="69"/>
        <source>Playlists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Search.qml" line="78"/>
        <source>Artists</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../qml/pages/Settings.qml" line="25"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="32"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="41"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="90"/>
        <source>Theme:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="100"/>
        <source>Dark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="100"/>
        <source>Light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="100"/>
        <source>System</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="149"/>
        <source>Quality:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="159"/>
        <source>low: prefer data over quality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="159"/>
        <source>medium</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="159"/>
        <source>high: prefer quality over data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="208"/>
        <source>Error song:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="218"/>
        <source>silence: 3 seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="218"/>
        <source>error: usefull to know what happens</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="266"/>
        <source>Account:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="275"/>
        <source>Log out</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TrackListItem</name>
    <message>
        <location filename="../qml/components/TrackListItem.qml" line="55"/>
        <source>popularity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/TrackListItem.qml" line="63"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/TrackListItem.qml" line="74"/>
        <source>Play</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/TrackListItem.qml" line="82"/>
        <source>End Queue</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UserMetricsHelper</name>
    <message>
        <location filename="../qml/services/UserMetricsHelper.qml" line="15"/>
        <source>tracks played today</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/services/UserMetricsHelper.qml" line="16"/>
        <source>No tracks played today</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
