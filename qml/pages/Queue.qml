import QtQuick 2.12
import Ubuntu.Components 1.3
import QtMultimedia 5.12

import "../components"


Page {

    property var player

    function refresh() {
        queue.model = spotSession.getAlbum('queue').size;
    }

    function focusCurrentTrack() {
        queue.currentIndex = player.playlist.currentIndex;
        queue.positionViewAtIndex(queue.currentIndex, ListView.Beginning);
    }

    Component.onCompleted: {
        player.changes.connect(function() {
            refresh();
            focusCurrentTrack();
        })
    }

    header: PageHeader {
        id: bottomEdgeHeader
        title: qsTr("Queue") + " " + Math.min((player.playlist.currentIndex + 1), player.playlist.itemCount) + " / " + player.playlist.itemCount
        trailingActionBar.numberOfSlots: 5
        trailingActionBar.actions: [
            Action {
                text: qsTr("Next")
                iconName: "media-seek-forward"
                enabled: player.playlist.itemCount > 0 && player.playlist.currentIndex < player.playlist.itemCount - 1
                onTriggered: {
                    player.playlist.next();
                }
            },
            Action {
                text: qsTr("Play/Pause")
                iconName: player.playbackState == Audio.PlayingState ? "media-playback-pause" : "media-playback-start"
                enabled: player.playlist.itemCount > 0
                onTriggered: {
                    if (player.playbackState != Audio.PlayingState) {
                        player.play();
                    } else {
                        player.pause();
                    }
                }
            },
            Action {
                text: qsTr("Precedent")
                iconName: "media-seek-backward"
                enabled: player.playlist.itemCount > 0 && player.playlist.currentIndex > 0
                onTriggered: {
                    player.playlist.previous();
                }
            },
            Action {
                text: qsTr("Clear")
                iconName: "toolkit_input-clear"
                enabled: player.playlist.itemCount > 0
                onTriggered: {
                    player.clear();
                    refresh();
                }
            },
            Action {
                text: qsTr("Repeat")
                iconName: player.playlist.playbackMode == Playlist.CurrentItemInLoop ? "media-playlist-repeat-one" :
                    player.playlist.playbackMode == Playlist.Sequential ? "media-playlist" :
                    player.playlist.playbackMode == Playlist.Loop ? "media-playlist-repeat" :
		            "media-playlist-shuffle"
                enabled: player.playlist.itemCount > 0
                onTriggered: {
                    // No Random if there is less than two tracks in the playlist
                    if (player.playlist.itemCount <= 1 && player.playlist.playbackMode == Playlist.Loop) {
                        player.playlist.playbackMode = Playlist.Sequential;
                    } else {
                        if (player.playlist.playbackMode == Playlist.Sequential) {
                            player.playlist.playbackMode = Playlist.CurrentItemInLoop;
                        } else if (player.playlist.playbackMode == Playlist.CurrentItemInLoop) {
                            player.playlist.playbackMode = Playlist.Loop;
                        } else if (player.playlist.playbackMode == Playlist.Loop) {
                            player.playlist.playbackMode = Playlist.Random;
                        } else {
                            player.playlist.playbackMode = Playlist.Sequential;
                        }
                    }
                }
            }
        ]
    }

    ListView {
        id: queue
        anchors.top: bottomEdgeHeader.bottom
        width: parent.width
        height: parent.height - bottomEdgeHeader.height
        clip: true
        model: spotSession.getAlbum('queue').size
        delegate: TrackListItem {
            id: trackListItem
            track: spotSession.getTrackByAlbum('queue', index)
            canDelete: true
            activeSimpleClick: true
            onPlayTrack: {
                player.playlist.currentIndex = index;
            }
            onEndQueue: {
                player.appendTrack(track)
            }
            onDeleteTrack: {
                if (player.playlist.currentIndex == index && player.playbackState == Audio.PlayingState) {
                    player.stop();
                }
                player.playlist.removeItem(index);
                spotSession.deleteQueue(index);
                refresh();
            }
        }
        currentIndex: Math.min((player.playlist.currentIndex + 1), player.playlist.itemCount)
        highlight:  Rectangle {
            width: queue.width
            height: units.gu(10)
            color: UbuntuColors.green
            y: queue.currentItem == null ? -1 : queue.currentItem.y
            Behavior on y {
                SpringAnimation {
                    spring: 3
                    damping: 0.2
                }
            }
        }
        highlightFollowsCurrentItem: false
        focus: true
    }

}
