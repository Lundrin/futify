import QtQuick 2.12
import Ubuntu.Components 1.3

Page {
    header: PageHeader {
        id: header
        title: qsTr("About")
        StyleHints {
            dividerColor: UbuntuColors.green
        }
    }

    Flickable {
        anchors.top: header.bottom
        width: parent.width
        height: parent.height - header.height
        contentWidth: parent.width
        contentHeight: units.gu(60)
        Column {
            spacing: units.gu(4)
            padding: units.gu(2)

            Column {
                spacing: units.gu(2)
                padding: units.gu(1)
                Label { 
                    text: qsTr('App:')
                    textSize: Label.Large
                }
                Column {
                    spacing: units.gu(2)
                    padding: units.gu(1)
                    Label {
                        text: qsTr('Futify is an unofficial native spotify client.')
                    }
                    Button {
                        iconName: 'stock_website'
                        text: 'gitlab.com/frenchutouch/futify'
                        width: units.gu(40)
                        onClicked: {
                            Qt.openUrlExternally('https://gitlab.com/frenchutouch/futify')
                        }
                    }
                    Button {
                        iconName: 'preferences-desktop-login-items-symbolic'
                        text: qsTr('issues')
                        width: units.gu(40)
                        onClicked: {
                            Qt.openUrlExternally('https://gitlab.com/frenchutouch/futify/-/issues')
                        }
                    }
                }
            }

            Column {
                spacing: units.gu(2)
                padding: units.gu(1)
                Label { 
                    text: qsTr('Author:')
                    textSize: Label.Large
                }
                Column {
                    spacing: units.gu(2)
                    padding: units.gu(1)
                    Label {
                        text: 'Romain Maneschi'
                    }
                    Button {
                        iconName: 'stock_website'
                        text: 'romain.maneschi.fr'
                        width: units.gu(40)
                        onClicked: {
                            Qt.openUrlExternally('http://romain.maneschi.fr')
                        }
                    }
                    Button {
                        iconName: 'twitter-symbolic'
                        text: '@RmManeschi'
                        width: units.gu(40)
                        onClicked: {
                            Qt.openUrlExternally('https://twitter.com/RmManeschi/')
                        }
                    }
                }
            }
        }
    }

}